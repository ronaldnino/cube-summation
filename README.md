# Léeme #

La tecnologia selecionada para resolver el desafío de codificación fue java. La solución fue implementada con la ayuda del framework spring-boot y bajo la arquitectura de api rest
que espone el recurso POST /testCases.

### Las capas de la aplicación ###

* Modelo (Operation.java, TCase.java)
* Servicio (CubeSummationService.java)
* Controlador (IndexController.java)

### La responsabilidad de cada clase creada ###

* IndexController.java: Esta clase tiene la resposabilidad de getionar las solicitudes realizadas y comunicarse con el servicio CubeSummationService.
* Operation.java: Esta clase representa abstracción de las operaciones del cube summation. Y en esta se defina las operaciones de las misma.
* TCase.java: Esta clase representa la abstracción de los casos de prueba del cube summation Y en esta se define los casos de prueba de las misma
* CubeSummationService.java: Esta clase realizar la suma del cubo y pasarsela al IndexControler


# Instrucciones de ejecución #

### Requerimientos ###

* Java 1.8 o superior (Obligatorio)
* Maven (Opcional)

###Si solo gusta ejecutar el jar, clone el proyecto en su maquina y acceda al directorio dist y ejecute el siguiente comando java -jar cube-sumation.###

[![Ejecución del jar](https://firebasestorage.googleapis.com/v0/b/datos-a78c7.appspot.com/o/Captura%20de%20pantalla%20de%202018-01-24%2002-45-42.png?alt=media&token=0ced1de0-e1ab-4f15-9518-19c599fd0ced)]

###Sigusta ejecutarlo con maven requiere ejecutar los siguiente comandos:###
```r
mvn clean install
mvn sprint-boot:run
```

### Para consumir el recurso instale un cliente rest. Recomiendo post, Siga las configuración de la imagen.###
[![Peticion por postman](https://firebasestorage.googleapis.com/v0/b/datos-a78c7.appspot.com/o/Captura%20de%20pantalla%20de%202018-01-24%2002-46-02.png?alt=media&token=bf4a63bd-c93b-4c4d-a2b4-2092b84e24bb)]

