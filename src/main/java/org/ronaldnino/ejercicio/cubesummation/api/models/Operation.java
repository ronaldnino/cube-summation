package org.ronaldnino.ejercicio.cubesummation.api.models;

import org.ronaldnino.ejercicio.cubesummation.api.enumerations.OperationType;

/**
 * Esta clase es la abstracción para Operación en suma de cubos
 * @author Ronald Eduardo Niño Guerra
 */

public class Operation {
    
    private OperationType operationType;
    private byte x1;
    private byte y1;
    private byte z1;
    private byte x2;
    private byte y2;
    private byte z2;
    private long  w;
    
    public Operation () {
    }
    
    /**
     * Este constructor se usa cuando necesita crear una operación de actualización
     * 
     * @param x
     * @param y
     * @param z
     * @param w
     */
    public Operation (byte x, byte y, byte z, int w) {
        this.operationType = OperationType.UPDATE;
        this.x1 = x;
        this.y1 = y;
        this.z1 = z;
        this.w  = w;
    }
    
    /**
     * Este constructor se usa cuando necesita crear una Operación de consulta
     * 
     * @param x1
     * @param y1
     * @param z1
     * @param x2
     * @param y2
     * @param z2
     */
    public Operation (byte x1, byte y1, byte z1, byte x2, byte y2, byte z2) {
        this.operationType = OperationType.QUERY;
        this.x1 = x1;
        this.y1 = y1;
        this.z1 = z1;
        this.x2 = x2;
        this.y2 = y2;
        this.z2 = z2;
    }

    /**
     * @return El tipo de operacion
     */
    public OperationType getOperationType() {
        return operationType;
    }

    /**
     * Establece el tipo de operacion
     * @param operationType
     */
    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    /**
     * @return El x1
     */
    public byte getX1() {
        return x1;
    }

    /**
     * Establece el valor a x1
     * @param x1
     */
    public void setX1(byte x1) {
        this.x1 = x1;
    }

    /**
     * @return El y1
     */
    public byte getY1() {
        return y1;
    }

    /**
     * Establece el valor a y1
     * @param y1
     */
    public void setY1(byte y1) {
        this.y1 = y1;
    }

    /**
     * @return El z1
     */
    public byte getZ1() {
        return z1;
    }

    /**
     * Establece el valor de z1
     * @param z1
     */
    public void setZ1(byte z1) {
        this.z1 = z1;
    }

    /**
     * @return El x2
     */
    public byte getX2() {
        return x2;
    }

    /**
     * Establece el valor de x2
     * @param x2
     */
    public void setX2(byte x2) {
        this.x2 = x2;
    }

    /**
     * @return El y2
     */
    public byte getY2() {
        return y2;
    }

    /**
     * Establece el valor de y2
     * @param y2
     */
    public void setY2(byte y2) {
        this.y2 = y2;
    }

    /**
     * @return El z2
     */
    public byte getZ2() {
        return z2;
    }

    /**
     * Establece el valor de z2
     * @param z2
     */
    public void setZ2(byte z2) {
        this.z2 = z2;
    }

    /**
     * @return El w
     */
    public long getW() {
        return w;
    }

    /**
     * Establece el valor a w
     * @param w
     */
    public void setW(int w) {
        this.w = w;
    }
    /**
     * Establece los valores segun el tipo de operacion
     * @param operation 
     */
    public void setValues(String[] operation) {
        if (operation[0].equals("UPDATE")) {
            this.operationType = OperationType.UPDATE;
            this.x1 = Byte.parseByte(operation[1]);
            this.y1 = Byte.parseByte(operation[2]);
            this.z1 = Byte.parseByte(operation[3]);
            this.w = Integer.parseInt(operation[4]);
        } else {
            this.operationType = OperationType.QUERY;
            this.x1 = Byte.parseByte(operation[1]);
            this.y1 = Byte.parseByte(operation[2]);
            this.z1 = Byte.parseByte(operation[3]);
            this.x2 = Byte.parseByte(operation[4]);
            this.y2 = Byte.parseByte(operation[5]);
            this.z2 = Byte.parseByte(operation[6]);
        }
    }
    
}
