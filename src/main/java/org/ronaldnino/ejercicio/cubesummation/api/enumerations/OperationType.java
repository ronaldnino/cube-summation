package org.ronaldnino.ejercicio.cubesummation.api.enumerations;

/**
 *
 * @author Ronald Eduardo Niño Guerra
 */
public enum OperationType {
    
    UPDATE, QUERY;
    
}