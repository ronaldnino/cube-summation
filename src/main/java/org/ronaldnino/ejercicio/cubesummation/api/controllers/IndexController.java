
package org.ronaldnino.ejercicio.cubesummation.api.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.ronaldnino.ejercicio.cubesummation.api.models.TCase;
import org.ronaldnino.ejercicio.cubesummation.api.services.CubeSummationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ronald Niño
 */
@RestController
public class IndexController {
    @Autowired
    private CubeSummationService cubeSummationService;
    

    @PostMapping(value = "/testCases", consumes = MediaType.TEXT_PLAIN_VALUE)
    public List<Long> testCases (@RequestBody String input) throws IOException {
        
        //I could persist this information with dao layer in order to generate a trace
        TCase[] tCases = cubeSummationService.testCases(input);
        List<Long> results = new ArrayList<>();
        for (TCase tCase : tCases) {
            results.addAll(tCase.getQueryResults());
        }
        return results;
    }
}
