package org.ronaldnino.ejercicio.cubesummation.api.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Ronald Eduardo Niño Guerra
 */
public class TCase {
    
    private Operation[] operations;
    private Map<Byte, Map<Byte,Map<Byte, Long>>> cube = new TreeMap<>();
    private Byte dimension;
    private List<Long> queryResults;

    public TCase(final short mOperations) {
        this.queryResults = new ArrayList<>();
        operations = new Operation[mOperations];
    }
    
    public TCase() {
        this.queryResults = new ArrayList<>();
    }
    
    /**
     *
     * @return true si la dimensión del cubo es válida
     */
    public boolean isCubeSizeValid() {
        return getDimension() > 0 && getDimension() <= 100;
    }
    
    /**
     * Establecer condiciones para los casos de pruebas
     * @param caseConditions
     */
    public void setConditions(String[] caseConditions) {
        setOperations(new Operation[Short.parseShort(caseConditions[1])]);
        setDimension(Byte.parseByte(caseConditions[0]));
    }

    /**
     *
     * @return true si el valor de las operaciones es válido
     */
    public boolean isOperationsLengthValid() {
        return getOperations().length > 0 && getOperations().length <= 1000;
    }

    /**
     *
     * @param operation
     * @return true si es válida la dimensión en la operación de actualización
     */
    public boolean areDimensionsValid (Operation operation) {
        return  0 < operation.getX1() && operation.getX1() <= getDimension() && 
                    0 < operation.getY1() && operation.getY1() <= getDimension() && 
                        0 < operation.getZ1() && operation.getZ1() <= getDimension();
                
    }
    
    /**
     *
     * @param operation
     * @return true si es valido el valor de actualización
     */
    public boolean isUpdateValueValid(Operation operation) {
        return  -1000000000 <= operation.getW() && operation.getW() <= 1000000000;
                
    }

    /**
     * @return Las operaciones
     */
    public Operation[] getOperations() {
        return operations;
    }

    /**
     * @param operations Establece el valor al arreglo operaciones 
     */
    public void setOperations(Operation[] operations) {
        this.operations = operations;
    }

    /**
     * @return El cubo
     */
    public Map<Byte, Map<Byte,Map<Byte, Long>>> getCube() {
        if (cube == null)
            return new TreeMap<>();
        return cube;
    }

    /**
     * Establece el valor al cubo
     * @param cube
     */
    public void setCube(Map cube) {
        this.cube = cube;
    }

    
    /**
     * Establece el valor a dimension
     * @param dimension 
     */
    void setDimension(Byte dimension) {
        this.dimension = dimension;
    }

    /**
     * @return La dimension
     */
    public Byte getDimension() {
        return dimension;
    }
    
    /**
     *
     * @return El result
     */
    public List<Long> getQueryResults() {
        return this.queryResults;
    }

    /**
     *
     * @param xCoord
     * @return La cordenada x del cubo
     */
    public Map getCubeInX(Byte xCoord) {
        return cube.get(xCoord);
    }
    
    /**
     *
     * @param xCoord
        * @return true si la cordenada x existe en el cubo
     */
    public boolean existXOnCube (Byte xCoord) {
        return cube.containsKey(xCoord);
    }  
    
    /**
     *
     * @param xCoord
     * @param yCoord
     * @return obtener línea en x, y
     */
    public Map getCubeInXY (Byte xCoord, Byte yCoord) {
        return (Map) (getCubeInX(xCoord)).get(yCoord);
    }
    
    /**
     *
     * @param xCoord
     * @param yCoord
     * @return true si la línea existe en x, y
     */
    public boolean existXYOnCube (Byte xCoord, Byte yCoord) {
        return (getCubeInX(xCoord)).containsKey(yCoord);
    }
    
    /**
     *
     * @param xCoord
     * @param yCoord
     * @param zCoord
     * @return obtener escalar ubicado en x, y, z
     */
    public Integer getCubeInXYZ (Byte xCoord, Byte yCoord, Byte zCoord) {
        return (Integer) (getCubeInXY(xCoord, yCoord)).get(zCoord);
    }
    
    /**
     *
     * @param xCoord
     * @param yCoord
     * @param zCoord
     * @return true si el escalar existe en x, y, z
     */
    public boolean existXYZOnCube (Byte xCoord, Byte yCoord, Byte zCoord) {
        return (getCubeInXY(xCoord, yCoord)).containsKey(zCoord);
    }
    
    /**
     * Agrega x en la cordenada del cubo
     * @param xCoord
     */
    public void addXtoCube(Byte xCoord) {
        cube.put(xCoord, new TreeMap<>());
    }
    
    /**
     * Agrega una line en cubo
     * @param xCoord
     * @param yCoord
     */
    public void addYtoCube(Byte xCoord, Byte yCoord) {
        getCubeInX(xCoord).put(yCoord, new TreeMap<>());
    }
    
    /**
     * Agrega z en la cordenada del cubo
     * @param xCoord
     * @param yCoord
     * @param zCoord
     * @param wValue
     */
    public void setValueToCube(Byte xCoord, Byte yCoord, Byte zCoord, Long wValue) {
        getCubeInXY(xCoord,yCoord).put(zCoord, wValue);
    }

    /**
     * Remplaza cordenada en el cubo
     * @param xCoord
     * @param yCoord
     * @param zCoord
     * @param wValue
     */
    public void replaceValueToCube(Byte xCoord, Byte yCoord, Byte zCoord, Long wValue) {
        getCubeInXY(xCoord, yCoord).replace(zCoord, wValue);
    }

}
