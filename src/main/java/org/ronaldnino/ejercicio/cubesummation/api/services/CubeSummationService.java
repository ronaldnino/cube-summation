package org.ronaldnino.ejercicio.cubesummation.api.services;


import java.io.FileNotFoundException;
import java.io.IOException;
import org.ronaldnino.ejercicio.cubesummation.api.models.TCase;
/**
 *
 * @author Ronald Eduardo Niño Guerra
 */
public interface CubeSummationService  {

    TCase[] testCases(String input) throws FileNotFoundException, IOException;
    
}

